import React from "react";
import PropTypes from "prop-types";

import Button from "../Button/Button.jsx";

import "./Header.scss";

const Header = (props) => {
  const { handleModalImage, handleModalText } = props;

  return (
    <header className="inner header">
      <div className="header__wrapper">
        <Button onClick={handleModalImage}>Open First Modal</Button>
        <Button onClick={handleModalText}>Open Second Modal</Button>
      </div>
    </header>
  );
};

Header.propTypes = {
  handleModalImage: PropTypes.func,
  handleModalText: PropTypes.func,
};

export default Header;

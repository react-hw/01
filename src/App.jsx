import React from "react";
import { useState } from "react";

import Header from "./components/Header/Header.jsx";
import ModalText from "./components/ModalText/ModalText.jsx";
import ModalImage from "./components/ModalImage/ModalImage.jsx";
import Button from "./components/Button/Button.jsx";

function App() {
  const [isModalImage, setIsModalImage] = useState(false);
  const [isModalText, setIsModalText] = useState(false);

  const handleModalImage = () => {
    setIsModalImage((prevState) => !prevState);
  };

  const handleModalText = () => {
    setIsModalText((prevState) => !prevState);
  };

  return (
    <div className="container">
      <Header
        handleModalImage={handleModalImage}
        handleModalText={handleModalText}
      />

      {isModalImage && (
        <ModalImage handleClose={handleModalImage}>
          <div className="button__wrapper">
            <Button onClick={handleModalImage}>NO, CANCEL</Button>
            <Button onClick={handleModalImage}>YES, DELETE</Button>
          </div>
        </ModalImage>
      )}

      {isModalText && (
        <ModalText handleClose={handleModalText}>
          <Button onClick={handleModalText}>ADD TO FAVORITE</Button>
        </ModalText>
      )}
    </div>
  );
}

export default App;

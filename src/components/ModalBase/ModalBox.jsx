import React from "react";
import PropTypes from "prop-types"
import "./ModalBase.scss";

const ModalBox = ({ children }) => {
  return <div className="modal">{children}</div>;
};

ModalBox.propTypes = {
  children: PropTypes.any,
}

export default ModalBox;

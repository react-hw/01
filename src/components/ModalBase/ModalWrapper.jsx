import React from "react";
import PropTypes from "prop-types";

import "./ModalBase.scss";

const ModalWrapper = ({ children, onClick }) => {
  const onCloseModalVeil = (event) => {
    if (event.target.classList.contains("madal__wrapper")) {
      onClick();
    }
  };

  return (
    <div className="madal__wrapper" onClick={onCloseModalVeil}>
      {children}
    </div>
  );
};

ModalWrapper.propTypes = {
  onClick: PropTypes.func,
  children: PropTypes.any,
};

export default ModalWrapper;

import React from "react";
import PropTypes from "prop-types";

import Close from "./icons/close.svg?react";
import "./ModalClose.scss";

const ModalClose = ({ onClick }) => {
  return (
    <button type="button" className="modal__close" onClick={onClick}>
        <Close className="modal__close-xsvg"/>
    </button>
  );
};

ModalClose.propTypes = {
  onClick: PropTypes.func,
};

export default ModalClose;

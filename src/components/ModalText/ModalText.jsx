import React from "react";
import PropTypes from "prop-types";

import ModalWrapper from "../ModalBase/ModalWrapper.jsx";
import ModalBox from "../ModalBase/ModalBox.jsx";
import ModalClose from "../ModalBase/ModalClose.jsx";
import ModalContainer from "../ModalBase/ModalContainer.jsx";
import ModalBody from "../ModalBase/ModalBody.jsx";
import ModalFooter from "../ModalBase/ModalFooter.jsx";

const ModalText = (props) => {
  const { handleClose, children } = props;
  return (
    <ModalWrapper onClick={handleClose}>
      <ModalBox>
        <ModalClose onClick={handleClose} />
        <ModalContainer>
          <ModalBody
            bodyTitle='Add Product "NAME"'
            bodyText="Description for you product"
          />
          <ModalFooter>{children}</ModalFooter>
        </ModalContainer>
      </ModalBox>
    </ModalWrapper>
  );
};

ModalText.propTypes = {
  handleClose: PropTypes.func,
  children: PropTypes.any,
};

export default ModalText;

import React from "react";
import PropTypes from "prop-types";

import ModalWrapper from "../ModalBase/ModalWrapper.jsx";
import ModalBox from "../ModalBase/ModalBox.jsx";
import ModalClose from "../ModalBase/ModalClose.jsx";
import ModalContainer from "../ModalBase/ModalContainer.jsx";
import ModalBody from "../ModalBase/ModalBody.jsx";
import ModalFooter from "../ModalBase/ModalFooter.jsx";
import ModalHeader from "../ModalBase/ModalHeader.jsx";

import "./ModalImage.scss";

const ModalImage = (props) => {
  const { handleClose, children } = props;

  return (
    <ModalWrapper onClick={handleClose}>
      <ModalBox>
        <ModalClose onClick={handleClose} />
        <ModalContainer>
          <ModalHeader>
            <div className="modal-image__header-wrapper">
              <img
                className="modal-image__header-img"
                scr=""
                alt="Blank Image Template"
              />
            </div>
          </ModalHeader>
          <ModalBody
            bodyTitle="Product Delete!"
            bodyText="By clicking the “Yes, Delete” button, PRODUCT NAME will be deleted."
          />
          <ModalFooter>{children}</ModalFooter>
        </ModalContainer>
      </ModalBox>
    </ModalWrapper>
  );
};

ModalImage.propTypes = {
  handleClose: PropTypes.func,
  children: PropTypes.any,
};

export default ModalImage;

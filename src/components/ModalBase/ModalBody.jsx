import React from "react";
import PropTypes from "prop-types";

import "./ModalBase.scss";

const ModalBody = ({ bodyTitle, bodyText }) => {
  return (
    <div className="modal__body">
      {bodyTitle && <p className="modal__body-title">{bodyTitle}</p>}
      {bodyText && <p className="modal__body-text">{bodyText}</p>}
    </div>
  );
};

ModalBody.propTypes = {
  bodyTitle: PropTypes.string,
  bodyText: PropTypes.string,
};

export default ModalBody;
